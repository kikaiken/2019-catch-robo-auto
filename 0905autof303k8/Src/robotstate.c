/*
 * robotstate.c
 *
 *  Created on: 2019/09/13
 *      Author: tomok
 */

#include "robotstate.h"
//#include "microswitch.h"
#include "motor.h"
#include "encoder.h"
#include "stm32f3xx_hal.h"
#include "servomotor.h"
//#include "stopswitch.h"

//奥に向かう向きが正のDUTY
//位置をキャリブレーションする
void ResetPos(void){
	while(HAL_GPIO_ReadPin(MICROSW_Y_MIN_PORT, MICROSW_Y_MIN_PIN) == GPIO_PIN_SET){
		CoordinateSetDuty(-0.2f);
	}
	CoordinateSetDuty(0.0f);
}

//停止スイッチが押されたら初期位置に戻してもとに戻る
void StopPos(int stop_flag){
	ServoSetAngle(AUTO_SERVO1_ZERO_ANGLE,AUTO_SERVO1_ID);
	ServoSetAngle(AUTO_SERVO2_ZERO_ANGLE,AUTO_SERVO2_ID);
	ResetPos();
}
